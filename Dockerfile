FROM golang:1.12 as builder

# Install tooling
RUN apt-get update \
    && apt-get install -y curl git --no-install-recommends \
    \
    # "Installing dep" \
    && curl -fsSL -o /usr/local/bin/dep https://github.com/golang/dep/releases/download/v0.5.0/dep-linux-amd64 \
    && chmod +x /usr/local/bin/dep \
    \
    && mkdir -p /go/src/github.com/scalify

ARG JOLOKIA_EXPORTER_VERSION=1.2.1

# Build jolokia exporter
RUN cd /go/src/github.com/scalify \
    && git clone https://github.com/Scalify/jolokia_exporter.git jolokia_exporter \
    && cd jolokia_exporter \
    && git checkout ${JOLOKIA_EXPORTER_VERSION}

WORKDIR /go/src/github.com/scalify/jolokia_exporter/

RUN dep init \
    \
    # "Fetching dependencies..." \
    && dep ensure -vendor-only \
    \
    # "Building jolokia exporter..." \
    && CGO_ENABLED=0 go build -a -ldflags '-s' -installsuffix cgo -o bin/jolokia_exporter .

FROM scratch

EXPOSE 9422/tcp

COPY --from=builder /go/src/github.com/scalify/jolokia_exporter/bin/jolokia_exporter /usr/local/bin/jolokia_exporter

ENTRYPOINT ["/usr/local/bin/jolokia_exporter"]
