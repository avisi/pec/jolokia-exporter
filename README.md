# Jolokia Prometheus Exporter

[![pipeline status](https://gitlab.com/avisi/pec/jolokia-exporter/badges/master/pipeline.svg)](https://gitlab.com/avisi/pec/jolokia-exporter/commits/master)

> Jolokia Prometheus metrics exporter in a Docker image

This repository contains a Dockerfile with a Prometheus Jolokia Exporter installation.

## Table of Contents

- [Usage](#usage)
- [Contribute](#contribute)
- [Automated build](#automated build)
- [License](#license)

## Automated build

This image is build at least once a month automatically. All PR's are automatically build.

## Contribute

PRs accepted. All issues should be reported in the [Gitlab issue tracker](https://gitlab.com/avisi/pec/jolokia-exporter/issues).

## License

[MIT © Avisi B.V.](LICENSE)
